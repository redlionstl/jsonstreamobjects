srcdir = '.'
blddir = 'build'
VERSION = '0.0.1'

def set_options(opt):
  opt.tool_options('compiler_cxx')
  opt.tool_options('compiler_cc')

def configure(conf):
  conf.env.append_value('CCFLAGS', '-fPIC')
  conf.env.append_value('CFLAGS', '-fPIC')
  conf.check_tool('compiler_cxx')
  conf.check_tool('compiler_cc')
  conf.check_tool('node_addon')
  print(type(conf.env))
  print(conf.env)

def build(bld):
  obj = bld.new_task_gen('cxx', 'shlib', 'node_addon')
  obj.target = 'jsparser'
  obj.source = ['./src/jsparser.cc', './yajl/src/yajl_alloc.c', './yajl/src/yajl_buf.c', './yajl/src/yajl.c', './yajl/src/yajl_encode.c', './yajl/src/yajl_gen.c', './yajl/src/yajl_lex.c', './yajl/src/yajl_parser.c', 'yajl/src/yajl_tree.c', './yajl/src/yajl_version.c' ]
  obj.includes = ['.','./yajl/src','./yajl/src/api']
