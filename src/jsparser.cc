#define BUILDING_NODE_EXTENSION

#include <v8.h>
#include <node.h>
#include <stdio.h>
#include <yajl_parse.h>
#include <yajl_parser.h>
#include <cstring>
#include "jsparser.h"
#include "macros.h"

using namespace v8 ;
using namespace jsparser;

long JSParser::instanceCount = 0;
JSParser* JSParser::parentParser;
Persistent<FunctionTemplate> JSParser::subParserTemplate ;

Handle<Array> jsparser::propertyEnumerator(const AccessorInfo &info){
	HandleScope scope;
	JSParser* obj = JSParser::ObjectWrap::Unwrap<JSParser>(info.This());
	Handle<Array> properties = obj->getPropertyNames();
	return scope.Close(properties);
}

Handle<Value> jsparser::propertyGetter(Local<String> property, const AccessorInfo& info){
	JSParser* obj = JSParser::ObjectWrap::Unwrap<JSParser>(info.This());
	return obj->getProperty(property, info);
}

Handle<Value> jsparser::propertySetter(Local<String> property, Local<Value> value, const AccessorInfo& info){
	HandleScope scope;
	v8::String::AsciiValue str(property);
	return scope.Close(Undefined());
}

Handle<Value> jsparser::indexedGetter(uint32_t index, const AccessorInfo& info){
	JSParser* obj = JSParser::ObjectWrap::Unwrap<JSParser>(info.This());
	return obj->getIndexedProperty(index, info);
}

Handle<Value> jsparser::indexedSetter(uint32_t index, Local<Value> value, const AccessorInfo& info){
	HandleScope scope;

	return scope.Close(Undefined());
}


JSParser::JSParser(unsigned char* offsetBuffer, long offsetBufferSize){
	// This bit of magic makes V8 GC much more often.  Bad for performance but
	// Might give insight if we are having memory issues again.
	// NOT for production use.
//	if( instanceCount > 500){
//		V8::IdleNotification();
//	}
	arrayIndex = 0;
	findArrayIndex = 0;
	JSParser::instanceCount ++;
	JSParser::parentParser = NULL;
	bufferState = buffer_external;
	buffer = offsetBuffer;
	rfFlag = rf_none;
	bufferSize = offsetBufferSize;
	hand = NULL;
	resetParser();
	setCallbacks();
}

JSParser::JSParser(const Arguments& args) {
	HandleScope scope;
	// Constructor for Javascript
	parserType = type_object;
	arrayIndex = 0;
	findArrayIndex = 0;
	JSParser::instanceCount ++;
	JSParser::parentParser = NULL;
	bufferState = buffer_internal;
	buffer = NULL;
	rfFlag = rf_none;
	bufferSize = 0;
	hand = NULL;

	resetParser();
	setCallbacks();


	if(args.Length() > 0){
		if(args[0]->IsString()){
			Local<String> stringVal = args[0]->ToString();
			bufferSize = stringVal->Utf8Length();
			buffer = new unsigned char[bufferSize];
			stringVal->WriteUtf8((char*)buffer, bufferSize);
			v8::V8::AdjustAmountOfExternalAllocatedMemory(bufferSize);
		} else {
			ThrowException(String::New("First param to JSParser() must be string"));
		}
	} else {
		ThrowException(String::New("First param to JSParser() must be string"));
	}

//	if(args.Length() == 2){
//		Local<Object> functionList = args[1]->ToObject();
//		for(int i = 0; i < 11; i++){
//			if(functionList->Get(String::New(callbackNames[i]))->IsFunction()){
//				Local<Object> obj = functionList->Get(String::New(callbackNames[i]))->ToObject();
//				// store the passed function and
//				js_callbacks[i] = v8::Persistent<Object>::New(obj);
//			}
//		}
//	}

};


JSParser::~JSParser() {
	JSParser::instanceCount --;
	yajl_free(hand);
	if(bufferState == buffer_internal){
		delete buffer;
		v8::V8::AdjustAmountOfExternalAllocatedMemory(-bufferSize);
	}
};

//Persistent<Object> JSParser::getCallback(int cb){
//	//return js_callbacks[cb];
//}


void JSParser::Init(Handle<Object> target) {
  // Prepare constructor template
  HandleScope scope;
  Local<FunctionTemplate> tpl = FunctionTemplate::New(New);
  tpl->SetClassName(String::NewSymbol("JSParser"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);
  tpl->InstanceTemplate()->SetNamedPropertyHandler(jsparser::propertyGetter, jsparser::propertySetter, NULL, NULL, jsparser::propertyEnumerator);

  Persistent<Function> constructor = Persistent<Function>::New(tpl->GetFunction());
  target->Set(String::NewSymbol("JSParser"), constructor);

  if(JSParser::subParserTemplate.IsEmpty()){
  		Handle<FunctionTemplate> subTpl = FunctionTemplate::New(NewInternal);
  		subTpl->SetClassName(String::NewSymbol("JSSubParser"));
  		subTpl->InstanceTemplate()->SetInternalFieldCount(1);

  		subTpl->InstanceTemplate()->SetIndexedPropertyHandler(indexedGetter, indexedSetter);
  		subTpl->InstanceTemplate()->SetNamedPropertyHandler(jsparser::propertyGetter, jsparser::propertySetter, NULL, NULL, jsparser::propertyEnumerator);
  		JSParser::subParserTemplate = Persistent<FunctionTemplate>::New(subTpl);
  	}


}

Handle<Value> JSParser::CreateSubObject() {
  HandleScope scope;

  JSParser::parentParser = this;
  Handle<Object> newParser = JSParser::subParserTemplate->GetFunction()->NewInstance(0, NULL);

  JSParser* cppParser = JSParser::ObjectWrap::Unwrap<JSParser>(newParser);
  cppParser->parserType = type_object;

  return scope.Close(newParser);
}

Handle<Value> JSParser::CreateSubArray() {
  HandleScope scope;

  JSParser::parentParser = this;
  Handle<Object> newParser = JSParser::subParserTemplate->GetFunction()->NewInstance(0, NULL);

  JSParser* cppParser = JSParser::ObjectWrap::Unwrap<JSParser>(newParser);
  cppParser->parserType = type_array;

  return scope.Close(newParser);
}


jsparser_states JSParser::popState(){
	if(targetStateIndex == 0){
		ThrowException(String::New("parser stack underflow"));
	}
	// intentional prefix decrement
	return targetStates[--targetStateIndex];
}

void JSParser::pushState(jsparser_states newState){
	if(targetStateIndex == PARSER_TARGET_STACK_SIZE){
		ThrowException(String::New("parser stack overflow"));
	}
	// intentional post increment
	targetStates[targetStateIndex++] = newState;
}

jsparser_states JSParser::peekState(){
	if(targetStateIndex == 0){
		ThrowException(String::New("parser stack state error"));
	}
	return targetStates[targetStateIndex -1];
}

int cbStartMap(void * ctx)
{
	JSParser* parser = (JSParser*) ctx;
	switch(parser->peekState()){
		case map_init:
			parser->popState();
			return handler_continue;
		case array_get_value:
			if(parser->arrayIndex == parser->findArrayIndex){
				parser->foundPropertyValue = parser->CreateSubObject();
				parser->setResultFlag(rf_found);
				parser->popState();
				return handler_stop;
			}
			parser->parseDepth++;
			parser->pushState(nested);
			return handler_continue;
			break;
		case map_get_value:
			// need the alternate jsparser constructor
//			parser->foundPropertyValue.Clear();
//			printf("Returning a map get value at start map\n");
			parser->foundPropertyValue = parser->CreateSubObject();
			parser->setResultFlag(rf_found);
			parser->popState();
			return handler_stop;
			break;
		case nested:
			parser->parseDepth ++;
			break;
		default:
			parser->parseDepth ++;
			parser->pushState(nested);
			break;
		break;
	}
	// unhandled state
	parser->setResultFlag(rf_none);
	return handler_continue;
}

static int cbNull(void * ctx)
{
	JSParser* parser = (JSParser*) ctx;
	switch(parser->peekState()){
		case map_get_value:
//			parser->foundPropertyValue.Clear();
//			printf("Found undefined");
			parser->foundPropertyValue = Undefined();
			parser->setResultFlag(rf_found);
			parser->popState();
			return handler_stop;
		break;
		case array_get_value:
			if(parser->arrayIndex == parser->findArrayIndex){
//				printf("Found undefined");
				parser->foundPropertyValue = Undefined();
				parser->setResultFlag(rf_found);
				parser->popState();
				return handler_stop;
			}
			parser->arrayIndex ++;
			return handler_continue;
		break;
		case map_get_keys:
		case nested:
			return handler_continue;
		break;
	}
	return handler_stop;
}

static int cbBoolean(void * ctx, int boolean)
{
	JSParser* parser = (JSParser*) ctx;
	switch(parser->peekState()){
		case map_get_value:
//			printf("Returning a boolean map_get_value\n");
			parser->foundPropertyValue = v8::BooleanObject::New(boolean);
			parser->setResultFlag(rf_found);
			parser->popState();
			return handler_stop;
		break;
		case array_get_value:
			if(parser->arrayIndex == parser->findArrayIndex){
//				printf("Returning a boolean array_get_value\n");
				parser->foundPropertyValue = v8::BooleanObject::New(boolean);
				parser->setResultFlag(rf_found);
				parser->popState();
				return handler_stop;
			}
			parser->arrayIndex ++;
			return handler_continue;
		break;
		case map_get_keys:
		case nested:
			return handler_continue;
		break;
	}
    return handler_stop;
}

static int cbNumber(void * ctx, const char * s, size_t l)
{
	JSParser* parser = (JSParser*) ctx;
	switch(parser->peekState()){
		case map_get_value:
//			parser->foundPropertyValue.Clear();
//			printf("Returning a number map_get_value\n");
			parser->foundPropertyValue = String::New((const char*)s, l)->ToNumber();
			parser->setResultFlag(rf_found);
			parser->popState();
			return handler_stop;
		break;
		case array_get_value:
			if(parser->arrayIndex == parser->findArrayIndex){
//				printf("Returning a number array_get_value\n");
				parser->foundPropertyValue = String::New((const char*)s, l)->ToNumber();
				parser->setResultFlag(rf_found);
				parser->popState();
				return handler_stop;
			}
			parser->arrayIndex ++;
			return handler_continue;
		break;
		case map_get_keys:
		case nested:
			return handler_continue;
		break;
	}
    return handler_continue;
}

static int cbString(void * ctx, const unsigned char * stringVal,
                           size_t stringLen)
{
	JSParser* parser = (JSParser*) ctx;
	switch(parser->peekState()){
		case map_get_value:
//			printf("Returning a string map_get_value\n");
			parser->foundPropertyValue = String::New((const char*)stringVal, stringLen);
			parser->setResultFlag(rf_found);
			parser->popState();
			return handler_stop;
		break;
		case array_get_value:
			if(parser->arrayIndex == parser->findArrayIndex){
//				printf("Returning a string array_get_value\n");
				parser->foundPropertyValue = String::New((const char*)stringVal, stringLen);
				parser->setResultFlag(rf_found);
				parser->popState();
				return handler_stop;
			}
			parser->arrayIndex ++;
			return handler_continue;
		break;
		case map_get_keys:
		case nested:
			return handler_continue;
		break;
	}
    return handler_continue;
}

static int cbMapKey(void * ctx, const unsigned char * stringVal,
                            size_t stringLen)
{
	JSParser* parser = (JSParser*) ctx;
	switch(parser->peekState()){
	case map_find_key:
//		printf("Parse depth %d\n", parser->parseDepth);
		if(memcmp((const char*)stringVal, (const char*)parser->findPropertyName, stringLen) == 0){
			parser->setResultFlag(rf_found);
			parser->popState();
			return handler_continue;
		}
		break;
	case map_get_keys:{
		HandleScope scope;
		Handle<Array> list = Handle<Array>(Array::Cast(*parser->foundPropertyValue));
		list->Set(list->Length(), String::New((const char*)stringVal, stringLen));
		return handler_continue;
	}
		break;
	case nested:
			return handler_continue;
		break;
	}
	return handler_continue;
}

static int cbEndMap(void * ctx)
{
	JSParser* parser = (JSParser*) ctx;
	switch(parser->peekState()){
		case nested:
			parser->parseDepth --;
			if(parser->parseDepth == 0){
				parser->popState();
				return handler_continue;
			}
			break;
		case map_get_keys:
			return handler_stop;
		break;
	}
	parser->setResultFlag(rf_none);
	return handler_continue;
}

static int cbEndArray(void * ctx)
{
	JSParser* parser = (JSParser*) ctx;
	switch(parser->peekState()){
		case nested:
			parser->parseDepth --;
			if(parser->parseDepth == 0){
				parser->popState();
				return handler_continue;
			}
			break;
		case map_get_keys:
			// should never get here.
			printf("How did we get here?");
		break;

	}
	parser->setResultFlag(rf_none);
//	printf("Found undefined");
	parser->foundPropertyValue = Undefined();
	return handler_stop;
}

static int cbStartArray(void * ctx)
{
	// This is the case where we create a (OBJECT) JSParser to return
	JSParser* parser = (JSParser*) ctx;

	switch(parser->peekState()){
		case array_get_value:
			if(parser->findArrayIndex != parser->arrayIndex){
				parser->arrayIndex ++ ;
				parser->parseDepth ++ ;
				parser->pushState(nested);
				return handler_continue;
			}
//			printf("Returning an array array_get_value\n");
			parser->foundPropertyValue = parser->CreateSubArray();
			parser->setResultFlag(rf_found);
			parser->popState();
			return handler_stop;
			break;
		case map_get_value:
//			printf("Returning an array map_get_value\n");
			parser->foundPropertyValue = parser->CreateSubArray();
			parser->setResultFlag(rf_found);
			parser->popState();
			return handler_stop;
		break;
		case array_init:
			// nothing to do here but acknowledge we found the start of array.
			parser->popState();
			return handler_continue;
		break;
		case nested:
			parser->parseDepth ++;
			break;
		default:
			parser->parseDepth ++;
			parser->pushState(nested);
			break;
	}
	return handler_continue;
}

void JSParser::setCallbacks(){
	callbacks.yajl_boolean = cbBoolean;
	callbacks.yajl_end_array = cbEndArray;
	callbacks.yajl_end_map = cbEndMap;
	callbacks.yajl_map_key = cbMapKey;
	callbacks.yajl_null = cbNull;
	callbacks.yajl_number = cbNumber;
	callbacks.yajl_start_array = cbStartArray;
	callbacks.yajl_start_map = cbStartMap;
	callbacks.yajl_string = cbString;
}

void JSParser::resetParser(){
	// Skip opening character.  Use case is that this is already an object or array
	if(this->hand != NULL){
		yajl_free(hand);
		hand = NULL;
	}
	this->hand = yajl_alloc(&callbacks, NULL, (void *) this);

	targetStateIndex = 0;
	parseDepth = 0;

	/* and let's allow comments by default */
	yajl_config(this->hand, yajl_allow_comments, 1);
	yajl_config(this->hand, yajl_allow_trailing_garbage, 1);

}

void JSParser::runParser(){
	yajl_parse(hand, buffer, bufferSize);
}

Handle<Array> JSParser::getPropertyNames(){
	HandleScope scope;
	//ThrowException(String::New("No, really"));
	foundPropertyValue = Array::New(0);
	if(parserType == type_array){
		return scope.Close(Handle<Array>(Array::Cast(*foundPropertyValue)));
	}
	pushState(map_get_keys);
	pushState(map_init);

	yajl_parse(hand, buffer, bufferSize);

	// The individual event handlers do not pop the state.
	popState();
	resetParser();

	return scope.Close(Handle<Array>(Array::Cast(*foundPropertyValue)));
}

Handle<Value> JSParser::getProperty(Local<String> property, const AccessorInfo& info){
	// To get a property, we need to ignore all events until we get a matching key
	// or the end of this object (or array)

	// Push states on in reverse order
	pushState(map_get_value); // Do Last
	pushState(map_find_key);
	pushState(map_init);	// Do first

	property->WriteUtf8((char*)findPropertyName, property->Utf8Length());

//	if(memcmp("inspect", findPropertyName, property->Utf8Length())==0){
//		return Undefined();
//	}
//	printf("Get Property name: %.*s\n", property->Utf8Length(), findPropertyName);
//	printf("Get Property name: %.*s\n%.*s\n", (int) property->Utf8Length(), findPropertyName, (int)bufferSize, buffer);
//	foundPropertyValue.Clear();

	foundPropertyValue = Undefined();

	yajl_parse(hand, buffer, bufferSize);

	resetParser();

	return foundPropertyValue;
}

Handle<Value> JSParser::getIndexedProperty(uint32_t index, const AccessorInfo& info){
	// To get a property, we need to ignore all events until we get a matching key
	// or the end of this object (or array)
//	printf("Looking for index: %d\n", index);
	if(parserType != type_array){
		return Undefined();
	}
	arrayIndex = 0;
	findArrayIndex = index;
	// Push states on in reverse order
	pushState(array_get_value); // Do Last
	pushState(array_init);
//printf("Find indexed\n");
	foundPropertyValue = Undefined();

	yajl_parse(hand, buffer, bufferSize);

	resetParser();

	return foundPropertyValue;
}

void JSParser::setResultFlag(rf_flag newFlag){
	rfFlag = newFlag;
}


Handle<Value> JSParser::New(const Arguments& args) {
  JSParser* obj = new JSParser(args);
  obj->Wrap(args.This());
  return args.This();
}

Handle<Value> JSParser::NewInternal(const Arguments& args) {
  JSParser* obj = new JSParser(JSParser::parentParser->buffer + (JSParser::parentParser->hand->bytesConsumed - 1), JSParser::parentParser->bufferSize - (JSParser::parentParser->hand->bytesConsumed - 1));
  obj->Wrap(args.This());
  return args.This();
}



void InitAll(Handle<Object> target) {
  JSParser::Init(target);
}

NODE_MODULE(jsparser, InitAll)
