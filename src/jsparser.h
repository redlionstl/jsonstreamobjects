#ifndef JSPARSER_H
#define JSPARSER_H

#include <v8.h>
#include <node.h>
#include <yajl_parse.h>
#include <yajl_gen.h>
using namespace v8;
namespace jsparser {


Handle<Array> propertyEnumerator(const AccessorInfo &info);

Handle<Value> propertyGetter(Local<String> property, const AccessorInfo& info);

Handle<Value> propertySetter(Local<String> property, Local<Value> value, const AccessorInfo& info);

Handle<Value> indexedGetter(uint32_t index, const AccessorInfo& info);

Handle<Value> indexedSetter(uint32_t index, Local<Value> value, const AccessorInfo& info);

const char* callbackNames[11] = { "null", "boolean", "integer", "double", "number", "string", "start_map", "map_key", "end_map", "start_array", "end_array"};
//enum lookupNames { cb_null, cb_boolean, cb_integer, cb_double, cb_number, cb_string, cb_start_map, cb_map_key, cb_end_map, cb_start_array, cb_end_array };
enum buffer_state { buffer_internal, buffer_external };
enum rf_flag { rf_error, rf_found, rf_none } ;
enum jsparser_states { map_init, map_find_key, map_get_value, map_get_keys, array_init, array_get_value, nested };
enum parser_types { type_array, type_object };
enum { handler_stop = 0, handler_continue = 1};

#define PARSER_TARGET_STACK_SIZE 5

class JSParser : public node::ObjectWrap {
 public:
  static void Init(v8::Handle<v8::Object> target);

//  Persistent<v8::Object> getCallback(int cb);
  Handle<Value> getProperty(Local<String> property, const AccessorInfo& info);
  Handle<Value> getIndexedProperty(uint32_t index, const AccessorInfo& info);
  void setResultFlag(rf_flag newFlag);
  jsparser_states popState();
  jsparser_states peekState();
  void pushState(jsparser_states newState);

  parser_types parserType ;
  int parseDepth;
  uint32_t findArrayIndex;
  uint32_t arrayIndex;
  unsigned char findPropertyName[256];
  Handle<Value> foundPropertyValue;
  static JSParser* parentParser ;
  static long instanceCount ;
  Handle<Array> getPropertyNames();
  Handle<Value> CreateSubObject();
  Handle<Value> CreateSubArray();
  static Persistent<FunctionTemplate> subParserTemplate ;
 private:

  jsparser_states targetStates[PARSER_TARGET_STACK_SIZE];
  int targetStateIndex;
   rf_flag rfFlag;
  yajl_handle hand;
  yajl_callbacks callbacks;
  buffer_state bufferState;
  unsigned char* buffer ;
  long bufferSize;
  //Persistent<Object> js_callbacks[11];


  JSParser(unsigned char* offsetBuffer, long offsetBufferSize);
  JSParser(const Arguments& args);
  ~JSParser();


  void setCallbacks();
  void resetParser();
  void runParser();

  static v8::Handle<v8::Value> New(const v8::Arguments& args);
  static v8::Handle<v8::Value> NewInternal(const v8::Arguments& args);

  static void disposeJSParser(v8::Persistent<v8::Value>, void * obj);
};
}
#endif
